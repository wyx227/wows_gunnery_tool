﻿namespace WoWs_Info_App
{
    partial class Accuracy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tBox_TimeStep = new System.Windows.Forms.TextBox();
            this.tBox_AngleStep = new System.Windows.Forms.TextBox();
            this.tBox_StartHt = new System.Windows.Forms.TextBox();
            this.tBox_TargetHt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tBox_TimeStep
            // 
            this.tBox_TimeStep.Location = new System.Drawing.Point(122, 52);
            this.tBox_TimeStep.Name = "tBox_TimeStep";
            this.tBox_TimeStep.Size = new System.Drawing.Size(231, 26);
            this.tBox_TimeStep.TabIndex = 0;
            // 
            // tBox_AngleStep
            // 
            this.tBox_AngleStep.Location = new System.Drawing.Point(122, 113);
            this.tBox_AngleStep.Name = "tBox_AngleStep";
            this.tBox_AngleStep.Size = new System.Drawing.Size(231, 26);
            this.tBox_AngleStep.TabIndex = 1;
            // 
            // tBox_StartHt
            // 
            this.tBox_StartHt.Location = new System.Drawing.Point(122, 178);
            this.tBox_StartHt.Name = "tBox_StartHt";
            this.tBox_StartHt.Size = new System.Drawing.Size(231, 26);
            this.tBox_StartHt.TabIndex = 2;
            // 
            // tBox_TargetHt
            // 
            this.tBox_TargetHt.Location = new System.Drawing.Point(122, 239);
            this.tBox_TargetHt.Name = "tBox_TargetHt";
            this.tBox_TargetHt.Size = new System.Drawing.Size(231, 26);
            this.tBox_TargetHt.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Time Step";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Angle Step";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Starting Height";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 242);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "Target Height";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BTN_Save
            // 
            this.BTN_Save.Location = new System.Drawing.Point(77, 285);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(135, 61);
            this.BTN_Save.TabIndex = 8;
            this.BTN_Save.Text = "Save";
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.button1_Click);
            // 
            // BTN_Cancel
            // 
            this.BTN_Cancel.Location = new System.Drawing.Point(218, 285);
            this.BTN_Cancel.Name = "BTN_Cancel";
            this.BTN_Cancel.Size = new System.Drawing.Size(135, 61);
            this.BTN_Cancel.TabIndex = 9;
            this.BTN_Cancel.Text = "Cancel";
            this.BTN_Cancel.UseVisualStyleBackColor = true;
            this.BTN_Cancel.Click += new System.EventHandler(this.BTN_Cancel_Click);
            // 
            // Accuracy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 358);
            this.Controls.Add(this.BTN_Cancel);
            this.Controls.Add(this.BTN_Save);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tBox_TargetHt);
            this.Controls.Add(this.tBox_StartHt);
            this.Controls.Add(this.tBox_AngleStep);
            this.Controls.Add(this.tBox_TimeStep);
            this.Name = "Accuracy";
            this.Text = "Accuracy";
            this.Load += new System.EventHandler(this.Accuracy_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tBox_TimeStep;
        private System.Windows.Forms.TextBox tBox_AngleStep;
        private System.Windows.Forms.TextBox tBox_StartHt;
        private System.Windows.Forms.TextBox tBox_TargetHt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_Cancel;
    }
}