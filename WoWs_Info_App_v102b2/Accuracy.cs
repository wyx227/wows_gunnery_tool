﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WoWs_Info_App
{
    public partial class Accuracy : Form
    {
        public Accuracy()
        {
            InitializeComponent();
        }

        private void Accuracy_Load(object sender, EventArgs e)
        {
            XmlDocument xm = new XmlDocument();
            xm.Load(@".\settings.xml");
            XmlNodeList Xn = xm.SelectNodes("//Value");
            tBox_TimeStep.Text = Xn[0].InnerText;
            tBox_AngleStep.Text = Xn[3].InnerText;
            tBox_StartHt.Text = Xn[1].InnerText;
            tBox_TargetHt.Text = Xn[2].InnerText;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            XmlDocument xm = new XmlDocument();
            xm.Load(@".\settings.xml");
            XmlNodeList xn = xm.SelectNodes("//Value");
            xn[0].InnerText = tBox_TimeStep.Text;
            xn[1].InnerText = tBox_StartHt.Text;
            xn[2].InnerText = tBox_TargetHt.Text;
            xn[3].InnerText = tBox_AngleStep.Text;
            xm.Save(@".\settings.xml");
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
