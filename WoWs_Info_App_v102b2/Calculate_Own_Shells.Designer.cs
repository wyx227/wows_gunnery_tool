﻿namespace WoWs_Info_App
{
    partial class Calculate_Own_Shells
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tBox_Caliber = new System.Windows.Forms.TextBox();
            this.tBox_V = new System.Windows.Forms.TextBox();
            this.tBox_Mass = new System.Windows.Forms.TextBox();
            this.tBox_Krupp = new System.Windows.Forms.TextBox();
            this.tBox_D = new System.Windows.Forms.TextBox();
            this.BTN_Calc = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.tBox_Range = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Result_V = new System.Windows.Forms.Label();
            this.Result_Pen = new System.Windows.Forms.Label();
            this.Ver_Pen = new System.Windows.Forms.Label();
            this.Ho_Pen = new System.Windows.Forms.Label();
            this.L_A = new System.Windows.Forms.Label();
            this.H_A = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(528, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Here you can calculate shell penetration of ships";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(12, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(415, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "that are not implemented in the game.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(46, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Caliber in mm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(218, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Initial Velocity in m/s";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(405, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Shell Weight in kg";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(227, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Air Drag";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(81, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Krupp Value";
            // 
            // tBox_Caliber
            // 
            this.tBox_Caliber.Location = new System.Drawing.Point(51, 144);
            this.tBox_Caliber.Name = "tBox_Caliber";
            this.tBox_Caliber.Size = new System.Drawing.Size(81, 26);
            this.tBox_Caliber.TabIndex = 7;
            // 
            // tBox_V
            // 
            this.tBox_V.Location = new System.Drawing.Point(231, 144);
            this.tBox_V.Name = "tBox_V";
            this.tBox_V.Size = new System.Drawing.Size(88, 26);
            this.tBox_V.TabIndex = 8;
            // 
            // tBox_Mass
            // 
            this.tBox_Mass.Location = new System.Drawing.Point(438, 144);
            this.tBox_Mass.Name = "tBox_Mass";
            this.tBox_Mass.Size = new System.Drawing.Size(81, 26);
            this.tBox_Mass.TabIndex = 9;
            // 
            // tBox_Krupp
            // 
            this.tBox_Krupp.Location = new System.Drawing.Point(85, 228);
            this.tBox_Krupp.Name = "tBox_Krupp";
            this.tBox_Krupp.Size = new System.Drawing.Size(81, 26);
            this.tBox_Krupp.TabIndex = 10;
            // 
            // tBox_D
            // 
            this.tBox_D.Location = new System.Drawing.Point(222, 228);
            this.tBox_D.Name = "tBox_D";
            this.tBox_D.Size = new System.Drawing.Size(81, 26);
            this.tBox_D.TabIndex = 11;
            // 
            // BTN_Calc
            // 
            this.BTN_Calc.Location = new System.Drawing.Point(183, 439);
            this.BTN_Calc.Name = "BTN_Calc";
            this.BTN_Calc.Size = new System.Drawing.Size(148, 56);
            this.BTN_Calc.TabIndex = 13;
            this.BTN_Calc.Text = "Calculate";
            this.BTN_Calc.UseVisualStyleBackColor = true;
            this.BTN_Calc.Click += new System.EventHandler(this.button2_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(46, 278);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(106, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Velocity at Hit";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(203, 278);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "Penetration";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(359, 278);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(160, 20);
            this.label.TabIndex = 16;
            this.label.Text = "Effective Vertical Pen";
            // 
            // tBox_Range
            // 
            this.tBox_Range.Location = new System.Drawing.Point(363, 228);
            this.tBox_Range.Name = "tBox_Range";
            this.tBox_Range.Size = new System.Drawing.Size(81, 26);
            this.tBox_Range.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(359, 205);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 20);
            this.label11.TabIndex = 18;
            this.label11.Text = "Range in m";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(46, 356);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(179, 20);
            this.label12.TabIndex = 19;
            this.label12.Text = "Effective Horizontal Pen";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(240, 356);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(128, 20);
            this.label13.TabIndex = 20;
            this.label13.Text = "Launching Angle";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(391, 356);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 20);
            this.label14.TabIndex = 21;
            this.label14.Text = "Hitting Angle";
            // 
            // Result_V
            // 
            this.Result_V.AutoSize = true;
            this.Result_V.Location = new System.Drawing.Point(47, 313);
            this.Result_V.Name = "Result_V";
            this.Result_V.Size = new System.Drawing.Size(75, 20);
            this.Result_V.TabIndex = 22;
            this.Result_V.Text = "Result_V";
            // 
            // Result_Pen
            // 
            this.Result_Pen.AutoSize = true;
            this.Result_Pen.Location = new System.Drawing.Point(203, 313);
            this.Result_Pen.Name = "Result_Pen";
            this.Result_Pen.Size = new System.Drawing.Size(92, 20);
            this.Result_Pen.TabIndex = 23;
            this.Result_Pen.Text = "Result_Pen";
            // 
            // Ver_Pen
            // 
            this.Ver_Pen.AutoSize = true;
            this.Ver_Pen.Location = new System.Drawing.Point(359, 313);
            this.Ver_Pen.Name = "Ver_Pen";
            this.Ver_Pen.Size = new System.Drawing.Size(71, 20);
            this.Ver_Pen.TabIndex = 24;
            this.Ver_Pen.Text = "Ver_Pen";
            // 
            // Ho_Pen
            // 
            this.Ho_Pen.AutoSize = true;
            this.Ho_Pen.Location = new System.Drawing.Point(47, 395);
            this.Ho_Pen.Name = "Ho_Pen";
            this.Ho_Pen.Size = new System.Drawing.Size(67, 20);
            this.Ho_Pen.TabIndex = 25;
            this.Ho_Pen.Text = "Ho_Pen";
            // 
            // L_A
            // 
            this.L_A.AutoSize = true;
            this.L_A.Location = new System.Drawing.Point(240, 395);
            this.L_A.Name = "L_A";
            this.L_A.Size = new System.Drawing.Size(38, 20);
            this.L_A.TabIndex = 26;
            this.L_A.Text = "L_A";
            // 
            // H_A
            // 
            this.H_A.AutoSize = true;
            this.H_A.Location = new System.Drawing.Point(391, 395);
            this.H_A.Name = "H_A";
            this.H_A.Size = new System.Drawing.Size(41, 20);
            this.H_A.TabIndex = 27;
            this.H_A.Text = "H_A";
            // 
            // Calculate_Own_Shells
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(569, 507);
            this.Controls.Add(this.H_A);
            this.Controls.Add(this.L_A);
            this.Controls.Add(this.Ho_Pen);
            this.Controls.Add(this.Ver_Pen);
            this.Controls.Add(this.Result_Pen);
            this.Controls.Add(this.Result_V);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tBox_Range);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.BTN_Calc);
            this.Controls.Add(this.tBox_D);
            this.Controls.Add(this.tBox_Krupp);
            this.Controls.Add(this.tBox_Mass);
            this.Controls.Add(this.tBox_V);
            this.Controls.Add(this.tBox_Caliber);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Calculate_Own_Shells";
            this.Text = "Calculate_Own_Shells";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tBox_Caliber;
        private System.Windows.Forms.TextBox tBox_V;
        private System.Windows.Forms.TextBox tBox_Mass;
        private System.Windows.Forms.TextBox tBox_Krupp;
        private System.Windows.Forms.TextBox tBox_D;
        private System.Windows.Forms.Button BTN_Calc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox tBox_Range;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label Result_V;
        private System.Windows.Forms.Label Result_Pen;
        private System.Windows.Forms.Label Ver_Pen;
        private System.Windows.Forms.Label Ho_Pen;
        private System.Windows.Forms.Label L_A;
        private System.Windows.Forms.Label H_A;
    }
}