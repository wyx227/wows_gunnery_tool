﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WoWs_Info_App
{
    public partial class Calculate_Own_Shells : Form
    {
        public Calculate_Own_Shells()
        {
            InitializeComponent();
            Load += new EventHandler(Calculate_Own_Shells_Load);
        }

        private string[] read_natural_constants()
        {
            string[] constants = new string[9];
            XmlDocument xm = new XmlDocument();
            xm.Load(@".\constants.xml");
            XmlNodeList Xn = xm.SelectNodes("//Value");
            for (int i = 0; i < Xn.Count; i++)
            {
                constants[i] = Xn[i].InnerText;
            }
            return constants;
        }

        private string[] read_settings()
        {
            string[] settings = new string[4];
            XmlDocument xm = new XmlDocument();
            xm.Load(@".\settings.xml");
            XmlNodeList Xn = xm.SelectNodes("//Value");
            for (int i = 0; i < Xn.Count; i++)
            {
                settings[i] = Xn[i].InnerText;
            }
            return settings;
        }

        public void Calculate_Own_Shells_Load(object sender, EventArgs e)
        {
            Result_V.Text = "";
            Result_Pen.Text = "";
            Ver_Pen.Text = "";
            Ho_Pen.Text = "";
            L_A.Text = "";
            H_A.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(tBox_Caliber.Text) < 0)
            {
                MessageBox.Show("Invalid!");
            }else if (Int32.Parse(tBox_V.Text) < 0)
            {
                MessageBox.Show("Invalid!");
            }else if (Int32.Parse(tBox_Mass.Text) < 0)
            {
                MessageBox.Show("Invalid!");
            }else if (Int32.Parse(tBox_Krupp.Text) < 0)
            {
                MessageBox.Show("Invalid!");
            }else if (Double.Parse(tBox_D.Text)>1 | Double.Parse(tBox_D.Text) < 0)
            {
                MessageBox.Show("Invalid!");
            }else if (Int32.Parse(tBox_Range.Text) < 0)
            {
                MessageBox.Show("Invalid!");
            }
            else
            {
                calculate_penetration(Int32.Parse(tBox_Caliber.Text), Int32.Parse(tBox_V.Text), Int32.Parse(tBox_Mass.Text), Int32.Parse(tBox_Krupp.Text), Int32.Parse(tBox_Range.Text), Double.Parse(tBox_D.Text));
            }
            //MessageBox.Show(Double.Parse(textBox5.Text).ToString());
        }

        private void calculate_penetration(int Caliber,int Initial_Velocity, int Shell_Weight,int Krupp,int range, double Air_Drag)
        {
            double cw_1 = Double.Parse(read_natural_constants()[7]);
            double cw_2;
            double t_0 = Double.Parse(read_natural_constants()[2]);
            double L = Double.Parse(read_natural_constants()[3]);
            double p_0 = Double.Parse(read_natural_constants()[4]);
            double R = Double.Parse(read_natural_constants()[5]);
            double M = Double.Parse(read_natural_constants()[6]);
            double rho = Double.Parse(read_natural_constants()[8]);
            double dt = Double.Parse(read_settings()[0]);
            double pi = 3.14;
            double gravity = Double.Parse(read_natural_constants()[1]);
            double vx;
            double vy;
            double P;
            double T;
            double Launching_Angle;

            double v;
            double Falling_Angle;


            double vx_check;
            double vy_check;
            double t_check = 0;
            double x_check = 0;
            double y_check = Double.Parse(read_settings()[1]);
            double T_check;
            double P_check;
            double rho_check;
            double max_range;

            double Area = pi * (Caliber / 2000.0) * (Caliber / 2000.0);
            double Coefficient = rho * Air_Drag / Shell_Weight * Area / 2.0;
            double Krupp_Calc = Krupp / 2400.0 * Double.Parse(read_natural_constants()[0]);
            cw_2 = 100 + 1000 / 3 * 0.001 * Caliber;

            vx_check = Initial_Velocity * Math.Cos(45 * pi / 180.0);
            vy_check = Initial_Velocity * Math.Sin(45 * pi / 180.0);

            do
            {
                x_check = x_check + dt * vx_check;
                y_check = y_check + dt * vy_check;

                T_check = t_0 - L * y_check;
                P_check = p_0 * Math.Pow((1 - L * y_check / t_0), (gravity * M / (R * L)));
                rho_check = P_check * M / (R * T_check);

                vx_check = vx_check - dt * Coefficient * rho_check * (cw_1 * Math.Pow(vx_check, 2) + cw_2 * vx_check);

                vy_check = vy_check - dt * gravity - dt * Coefficient * (cw_1 * Math.Pow(vy_check, 2) + cw_2 * Math.Abs(vy_check)) * Math.Sign(vy_check);

                t_check = t_check + dt;


            } while (y_check > -0.01);
            max_range = x_check;
            if (range > max_range)
            {
                MessageBox.Show("The given range is greater than the maximal range!");
                tBox_Range.Text = "";
            }
            else
            {
                int Angle = 45 * (int)(1 / Double.Parse(read_settings()[3])) + 1;
                for (int i = 1; i < Angle; i++)
                {
                    vx = Initial_Velocity * Math.Cos(i * Double.Parse(read_settings()[3]) * pi / 180.0);
                    vy = Initial_Velocity * Math.Sin(i * Double.Parse(read_settings()[3]) * pi / 180.0);

                    double t = 0;
                    double x = 0;
                    double y = Double.Parse(read_settings()[1]);
                    do
                    {
                        x = x + dt * vx;
                        y = y + dt * vy;

                        T = t_0 - L * y;
                        P = p_0 * Math.Pow((1 - L * y / t_0), (gravity * M / (R * L)));
                        rho = P * M / (R * T);

                        vx = vx - dt * Coefficient * rho * (cw_1 * Math.Pow(vx, 2) + cw_2 * vx);

                        vy = vy - dt * gravity - dt * Coefficient * (cw_1 * Math.Pow(vy, 2) + cw_2 * Math.Abs(vy)) * Math.Sign(vy);

                        t = t + dt;


                    } while (y > -0.01);
                    if (Math.Abs((x - range) / range) < 0.01)
                    {
                        Launching_Angle = Double.Parse(read_settings()[3]) * i;
                        v = Math.Sqrt(vx * vx + vy * vy);
                        Falling_Angle = Math.Abs(Math.Atan(Math.Abs(vy) / vx) * 180.0 / pi);
                        double penetration = Krupp_Calc * Math.Pow(v, 1.1) * Math.Pow(Shell_Weight, 0.55) / Math.Pow(Caliber, 0.65);
                        double verti_pen = penetration * Math.Sin(Math.Abs(Falling_Angle) * pi / 180.0);
                        Result_V.Text = Math.Round(v, 2).ToString() + " mm";
                        Result_Pen.Text = Math.Round(penetration, 2).ToString() + " mm";
                        Ver_Pen.Text = Math.Round(Math.Sqrt(penetration * penetration - verti_pen * verti_pen), 2).ToString() + " mm";
                        Ho_Pen.Text = Math.Round(verti_pen, 2).ToString() + " mm";
                        L_A.Text = Math.Round(Launching_Angle, 2).ToString() + " °";
                        H_A.Text = Math.Round(Falling_Angle, 2).ToString() + " °";
                    }
                }
            }

            
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
