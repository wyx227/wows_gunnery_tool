﻿namespace WoWs_Info_App
{
    partial class Comparing_Penetration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Ship_List_1 = new System.Windows.Forms.ListBox();
            this.Ship_List_2 = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BTN_Compare = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tBox_Range = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.V_1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Ho_Pen_1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.V_2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Ho_Pen_2 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.V_P_1 = new System.Windows.Forms.Label();
            this.V_P_2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(568, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "To compare the penetrating performance, choose two ships and give the range.";
            // 
            // Ship_List_1
            // 
            this.Ship_List_1.FormattingEnabled = true;
            this.Ship_List_1.ItemHeight = 20;
            this.Ship_List_1.Location = new System.Drawing.Point(12, 125);
            this.Ship_List_1.Name = "Ship_List_1";
            this.Ship_List_1.Size = new System.Drawing.Size(198, 324);
            this.Ship_List_1.TabIndex = 1;
            // 
            // Ship_List_2
            // 
            this.Ship_List_2.FormattingEnabled = true;
            this.Ship_List_2.ItemHeight = 20;
            this.Ship_List_2.Location = new System.Drawing.Point(777, 125);
            this.Ship_List_2.Name = "Ship_List_2";
            this.Ship_List_2.Size = new System.Drawing.Size(198, 324);
            this.Ship_List_2.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 81);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(198, 26);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(777, 81);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(198, 26);
            this.textBox2.TabIndex = 4;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ship 1:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(773, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ship 2:";
            // 
            // BTN_Compare
            // 
            this.BTN_Compare.Location = new System.Drawing.Point(359, 391);
            this.BTN_Compare.Name = "BTN_Compare";
            this.BTN_Compare.Size = new System.Drawing.Size(194, 58);
            this.BTN_Compare.TabIndex = 7;
            this.BTN_Compare.Text = "Compare";
            this.BTN_Compare.UseVisualStyleBackColor = true;
            this.BTN_Compare.Click += new System.EventHandler(this.BTN_Compare_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(324, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Range in m";
            // 
            // tBox_Range
            // 
            this.tBox_Range.Location = new System.Drawing.Point(328, 81);
            this.tBox_Range.Name = "tBox_Range";
            this.tBox_Range.Size = new System.Drawing.Size(203, 26);
            this.tBox_Range.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(228, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Velocity";
            // 
            // V_1
            // 
            this.V_1.AutoSize = true;
            this.V_1.Location = new System.Drawing.Point(228, 165);
            this.V_1.Name = "V_1";
            this.V_1.Size = new System.Drawing.Size(51, 20);
            this.V_1.TabIndex = 11;
            this.V_1.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(228, 223);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(179, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Effective Horizontal Pen";
            // 
            // Ho_Pen_1
            // 
            this.Ho_Pen_1.AutoSize = true;
            this.Ho_Pen_1.Location = new System.Drawing.Point(228, 254);
            this.Ho_Pen_1.Name = "Ho_Pen_1";
            this.Ho_Pen_1.Size = new System.Drawing.Size(51, 20);
            this.Ho_Pen_1.TabIndex = 13;
            this.Ho_Pen_1.Text = "label8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(675, 136);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 20);
            this.label9.TabIndex = 14;
            this.label9.Text = "Velocity";
            // 
            // V_2
            // 
            this.V_2.AutoSize = true;
            this.V_2.Location = new System.Drawing.Point(679, 165);
            this.V_2.Name = "V_2";
            this.V_2.Size = new System.Drawing.Size(60, 20);
            this.V_2.TabIndex = 15;
            this.V_2.Text = "label10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(579, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(179, 20);
            this.label11.TabIndex = 16;
            this.label11.Text = "Effective Horizontal Pen";
            // 
            // Ho_Pen_2
            // 
            this.Ho_Pen_2.AutoSize = true;
            this.Ho_Pen_2.Location = new System.Drawing.Point(679, 254);
            this.Ho_Pen_2.Name = "Ho_Pen_2";
            this.Ho_Pen_2.Size = new System.Drawing.Size(60, 20);
            this.Ho_Pen_2.TabIndex = 17;
            this.Ho_Pen_2.Text = "label12";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(228, 306);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(160, 20);
            this.label13.TabIndex = 18;
            this.label13.Text = "Effective Vertical Pen";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(594, 306);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(160, 20);
            this.label14.TabIndex = 19;
            this.label14.Text = "Effective Vertical Pen";
            // 
            // V_P_1
            // 
            this.V_P_1.AutoSize = true;
            this.V_P_1.Location = new System.Drawing.Point(232, 335);
            this.V_P_1.Name = "V_P_1";
            this.V_P_1.Size = new System.Drawing.Size(60, 20);
            this.V_P_1.TabIndex = 20;
            this.V_P_1.Text = "label15";
            // 
            // V_P_2
            // 
            this.V_P_2.AutoSize = true;
            this.V_P_2.Location = new System.Drawing.Point(679, 335);
            this.V_P_2.Name = "V_P_2";
            this.V_P_2.Size = new System.Drawing.Size(60, 20);
            this.V_P_2.TabIndex = 21;
            this.V_P_2.Text = "label16";
            // 
            // Comparing_Penetration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(987, 461);
            this.Controls.Add(this.V_P_2);
            this.Controls.Add(this.V_P_1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Ho_Pen_2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.V_2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Ho_Pen_1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.V_1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tBox_Range);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BTN_Compare);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Ship_List_2);
            this.Controls.Add(this.Ship_List_1);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Comparing_Penetration";
            this.Text = "Comparing Penetration";
            this.Load += new System.EventHandler(this.Comparing_Penetration_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox Ship_List_1;
        private System.Windows.Forms.ListBox Ship_List_2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BTN_Compare;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tBox_Range;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label V_1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Ho_Pen_1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label V_2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label Ho_Pen_2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label V_P_1;
        private System.Windows.Forms.Label V_P_2;
    }
}