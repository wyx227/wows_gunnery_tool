﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WoWs_Info_App
{
    public partial class Comparing_Penetration : Form
    {
        public Comparing_Penetration()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            Ship_List_1.SelectedItems.Clear();
            for (int i = Ship_List_1.Items.Count - 1; i >= 0; i--)
            {
                if (Ship_List_1.Items[i].ToString().ToLower().Contains(textBox1.Text.ToLower()))
                {
                    Ship_List_1.SetSelected(i, true);

                }
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            Ship_List_2.SelectedItems.Clear();
            for (int i = Ship_List_2.Items.Count - 1; i >= 0; i--)
            {
                if (Ship_List_2.Items[i].ToString().ToLower().Contains(textBox2.Text.ToLower()))
                {
                    Ship_List_2.SetSelected(i, true);

                }
            }
        }

        private string[] read_settings()
        {
            string[] settings = new string[4];
            XmlDocument xm = new XmlDocument();
            xm.Load(@".\settings.xml");
            XmlNodeList Xn = xm.SelectNodes("//Value");
            for (int i = 0; i < Xn.Count; i++)
            {
                settings[i] = Xn[i].InnerText;
            }
            return settings;
        }

        private double[] calculate_penetration(string[] parameter, int range)
        {
            double[] result = new double[3];

            int Caliber = Int32.Parse(parameter[0]);
            int Shell_Weight = Int32.Parse(parameter[2]);
            double Air_Drag = Double.Parse(parameter[3]) / 1000;
            int Initial_Velocity = Int32.Parse(parameter[1]);
            int Krupp = Int32.Parse(parameter[4]);
            string Nation = parameter[5];
            string Type = parameter[6];

            double cw_1 = Double.Parse(read_natural_constants()[7]);
            double cw_2;
            double t_0 = Double.Parse(read_natural_constants()[2]);
            double L = Double.Parse(read_natural_constants()[3]);
            double p_0 = Double.Parse(read_natural_constants()[4]);
            double R = Double.Parse(read_natural_constants()[5]);
            double M = Double.Parse(read_natural_constants()[6]);
            double rho = Double.Parse(read_natural_constants()[8]);
            double dt = Double.Parse(read_settings()[0]);
            double pi = 3.14;
            double gravity = Double.Parse(read_natural_constants()[1]);

            double vx;
            double vy;
            double P;
            double T;
            double Launching_Angle;

            double v;
            double Falling_Angle;

            double vx_check;
            double vy_check;
            double t_check = 0;
            double x_check = 0;
            double y_check = Double.Parse(read_settings()[1]);
            double T_check;
            double P_check;
            double rho_check;
            double max_range;

            double Area = pi * (Caliber / 2000.0) * (Caliber / 2000.0);
            double Coefficient = rho * Air_Drag / Shell_Weight * Area / 2.0;
            double Krupp_Calc = Krupp / 2400.0 * Double.Parse(read_natural_constants()[0]);
            cw_2 = 100 + 1000 / 3 * 0.001 * Caliber;

            vx_check = Initial_Velocity * Math.Cos(45 * pi / 180.0);
            vy_check = Initial_Velocity * Math.Sin(45 * pi / 180.0);

            do
            {
                x_check = x_check + dt * vx_check;
                y_check = y_check + dt * vy_check;

                T_check = t_0 - L * y_check;
                P_check = p_0 * Math.Pow((1 - L * y_check / t_0), (gravity * M / (R * L)));
                rho_check = P_check * M / (R * T_check);

                vx_check = vx_check - dt * Coefficient * rho_check * (cw_1 * Math.Pow(vx_check, 2) + cw_2 * vx_check);

                vy_check = vy_check - dt * gravity - dt * Coefficient * (cw_1 * Math.Pow(vy_check, 2) + cw_2 * Math.Abs(vy_check)) * Math.Sign(vy_check);

                t_check = t_check + dt;


            } while (y_check > -0.01);
            max_range = x_check;
            if (range > max_range)
            {
                MessageBox.Show("The given range is greater than the maximal range!");
                tBox_Range.Text = "";
                result[0] = -1;
                result[1] = -1;
                result[2] = -1;
                return result;
            }
            else
            {
                int Angle = 45 * (int)(1 / Double.Parse(read_settings()[3])) + 1;
                for (int i = 1; i < Angle; i++)
                {
                    vx = Initial_Velocity * Math.Cos(i * Double.Parse(read_settings()[3]) * pi / 180.0);
                    vy = Initial_Velocity * Math.Sin(i * Double.Parse(read_settings()[3]) * pi / 180.0);

                    double t = 0;
                    double x = 0;
                    double y = 0;
                    do
                    {
                        x = x + dt * vx;
                        y = y + dt * vy;

                        T = t_0 - L * y;
                        P = p_0 * Math.Pow((1 - L * y / t_0), (gravity * M / (R * L)));
                        rho = P * M / (R * T);

                        vx = vx - dt * Coefficient * rho * (cw_1 * Math.Pow(vx, 2) + cw_2 * vx);

                        vy = vy - dt * gravity - dt * Coefficient * (cw_1 * Math.Pow(vy, 2) + cw_2 * Math.Abs(vy)) * Math.Sign(vy);

                        t = t + dt;


                    } while (y > -0.01);
                    if (Math.Abs((x - range) / range) < 0.01)
                    {
                        Launching_Angle = Double.Parse(read_settings()[3]) * i;
                        v = Math.Sqrt(vx * vx + vy * vy);
                        Falling_Angle = Math.Abs(Math.Atan(Math.Abs(vy) / vx) * 180.0 / pi);
                        double penetration = Krupp_Calc * Math.Pow(v, 1.1) * Math.Pow(Shell_Weight, 0.55) / Math.Pow(Caliber, 0.65);
                        double verti_pen = penetration * Math.Sin(Math.Abs(Falling_Angle) * pi / 180.0);

                        if (!Angle_Analysis(Type, Nation, Math.Abs(Falling_Angle), Caliber))
                        {
                            result[0] = Math.Round(verti_pen, 2);

                        }
                        else
                        {
                            result[0] = Math.Floor(Caliber / 14.3);
                        }

                        result[1] = Math.Round(Math.Sqrt(penetration * penetration - verti_pen * verti_pen), 2);
                        result[2] = Math.Round(v, 2);

                    }
                }
                return result;
            }
         
        }

        private bool Angle_Analysis(string type, string nation, double angle, double caliber)
        {
            bool USN = false;
            bool HMS = false;
            bool Cruiser = false;
            bool Battleship = false;
            bool High_Caliber = false;
            bool bounce = false;

            if (type == "Cruiser")
            {

                Cruiser = true;
            }
            else if (type == "Battleship")
            {
                Battleship = true;
            }

            if (nation == "USN")
            {
                USN = true;
            }
            else if (nation == "HMS")
            {
                HMS = true;
            }

            if (caliber == 203 | caliber == 406)
            {
                High_Caliber = true;
            }

            if (USN && (Cruiser | Battleship) && High_Caliber)
            {
                if (angle < 22.5 - normalisation(caliber))
                {
                    bounce = true;
                }
                else
                {
                    bounce = false;
                }
            }
            else if (HMS && Cruiser && !High_Caliber)
            {
                if (angle < 15 - normalisation(caliber))
                {
                    bounce = true;
                }
                else
                {
                    bounce = false;
                }
            }
            else if (HMS && Battleship)
            {
                if (angle < 22.5 - normalisation(caliber))
                {
                    bounce = true;
                }
                else
                {
                    bounce = false;
                }
            }
            else
            {
                if (angle < 30 - normalisation(caliber))
                {
                    bounce = true;
                }
                else
                {
                    bounce = false;
                }
            }
            return bounce;
        }

        private string[] read_natural_constants()
        {
            string[] constants = new string[9];
            XmlDocument xm = new XmlDocument();
            xm.Load(@".\constants.xml");
            XmlNodeList Xn = xm.SelectNodes("//Value");
            for (int i = 0; i < Xn.Count; i++)
            {
                constants[i] = Xn[i].InnerText;
            }
            return constants;
        }

        private double normalisation(double caliber)
        {
            double normalisation;
            if (caliber < 131)
            {
                normalisation = 10;
            }
            else if (caliber < 153)
            {
                normalisation = 8.5;
            }
            else if (caliber < 221)
            {
                normalisation = 7;
            }
            else
            {
                normalisation = 6;
            }
            return normalisation;
        }

        private void BTN_Compare_Click(object sender, EventArgs e)
        {
            XmlDocument xm = new XmlDocument();
            string list = "//Name";
            string[] parameter = new string[7];
            string[] parameter2 = new string[7];
            xm.Load(@".\ships.xml");
            XmlNodeList Xn = xm.SelectNodes(list);
            XmlNodeList Xn_Caliber = xm.SelectNodes("//Caliber");
            XmlNodeList Xn_Initial_V = xm.SelectNodes("//Initial_Velocity");
            XmlNodeList Xn_Shell_W = xm.SelectNodes("//Shell_Weight");
            XmlNodeList Xn_Air_Drag = xm.SelectNodes("//Air_Drag");
            XmlNodeList Xn_Krupp = xm.SelectNodes("//Krupp");
            XmlNodeList Xn_Nation = xm.SelectNodes("//Nation");
            XmlNodeList Xn_Type = xm.SelectNodes("//Type");
            for (int i = 0; i < Xn.Count; i++)
            {
                if (Ship_List_1.GetItemText(Ship_List_1.SelectedItem) == Xn[i].InnerText)
                {
                    parameter[0] = Xn_Caliber[i].InnerText;
                    parameter[1] = Xn_Initial_V[i].InnerText;
                    parameter[2] = Xn_Shell_W[i].InnerText;
                    parameter[3] = Xn_Air_Drag[i].InnerText;
                    parameter[4] = Xn_Krupp[i].InnerText;
                    parameter[5] = Xn_Nation[i].InnerText;
                    parameter[6] = Xn_Type[i].InnerText;
                }
            }
            for (int i = 0; i < Xn.Count; i++)
            {
                if (Ship_List_2.GetItemText(Ship_List_2.SelectedItem) == Xn[i].InnerText)
                {
                    parameter2[0] = Xn_Caliber[i].InnerText;
                    parameter2[1] = Xn_Initial_V[i].InnerText;
                    parameter2[2] = Xn_Shell_W[i].InnerText;
                    parameter2[3] = Xn_Air_Drag[i].InnerText;
                    parameter2[4] = Xn_Krupp[i].InnerText;
                    parameter2[5] = Xn_Nation[i].InnerText;
                    parameter2[6] = Xn_Type[i].InnerText;
                }
            }
            if (Int32.Parse(tBox_Range.Text) < 0)
            {
                MessageBox.Show("Invalid range");
            }
            else
            {
                if (calculate_penetration(parameter2, Int32.Parse(tBox_Range.Text))[0] !=  -1)
                {
                    V_1.Text = calculate_penetration(parameter, Int32.Parse(tBox_Range.Text))[2].ToString() + " m/s";
                    Ho_Pen_1.Text = calculate_penetration(parameter, Int32.Parse(tBox_Range.Text))[0].ToString() + " mm";
                    V_P_1.Text = calculate_penetration(parameter, Int32.Parse(tBox_Range.Text))[1].ToString() + " mm";

                    V_2.Text = calculate_penetration(parameter2, Int32.Parse(tBox_Range.Text))[2].ToString() + " m/s";
                    Ho_Pen_2.Text = calculate_penetration(parameter2, Int32.Parse(tBox_Range.Text))[0].ToString() + " mm";
                    V_P_2.Text = calculate_penetration(parameter2, Int32.Parse(tBox_Range.Text))[1].ToString() + " mm";
                }
                

            }

        }

        private void Comparing_Penetration_Load(object sender, EventArgs e)
        {
            V_1.Text = "";
            V_2.Text = "";
            Ho_Pen_1.Text = "";
            Ho_Pen_2.Text = "";
            V_P_1.Text = "";
            V_P_2.Text = "";

            XmlDocument xm = new XmlDocument();
            string list = "//Name";

            xm.Load(@".\ships.xml");
            XmlNodeList Xn = xm.SelectNodes(list);

            foreach (XmlNode xNode in Xn)
            {
                Ship_List_1.Items.Add(xNode.InnerText);
                Ship_List_2.Items.Add(xNode.InnerText);
            }
        }
    }
}
