﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WoWs_Info_App
{
    public partial class Data_Management : Form
    {
        DataTable table = new DataTable("tbl");
        public Data_Management()
        {
            InitializeComponent();
        }
        private void Data_Management_Load(object sender, EventArgs e)
        {
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Type", typeof(string));
            table.Columns.Add("Tier", typeof(string));
            table.Columns.Add("Nation", typeof(string));
            table.Columns.Add("Caliber", typeof(int));
            table.Columns.Add("Shell Weight", typeof(int));
            table.Columns.Add("Initial Velocity", typeof(int));
            table.Columns.Add("Air Drag", typeof(double));
            table.Columns.Add("Krupp", typeof(int));

            dataGridView1.DataSource = table;
        }

        private void BTN_Load_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(@".\ships.xml");
            dataGridView1.DataSource = ds.Tables[0];
            MessageBox.Show("Data Imported");
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.WriteXml(@".\ships.xml");
            
            MessageBox.Show("Data Exported");
        }

        private void Data_Management_Load_1(object sender, EventArgs e)
        {

        }

        private void Data_Management_FormClose(object sender, FormClosedEventArgs e)
        {
            Dispose();
        }
    }
}
