﻿namespace WoWs_Info_App
{
    partial class IFHE_Tool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Ship_List = new System.Windows.Forms.ListBox();
            this.tBox_Ship_Name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.BTN_Search = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.HE_Pen_No_IFHE = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.HE_Pen_IFHE = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(256, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Should I get IFHE? Let me tell you.";
            // 
            // Ship_List
            // 
            this.Ship_List.FormattingEnabled = true;
            this.Ship_List.ItemHeight = 20;
            this.Ship_List.Location = new System.Drawing.Point(16, 95);
            this.Ship_List.Name = "Ship_List";
            this.Ship_List.Size = new System.Drawing.Size(151, 244);
            this.Ship_List.TabIndex = 1;
            // 
            // tBox_Ship_Name
            // 
            this.tBox_Ship_Name.Location = new System.Drawing.Point(63, 55);
            this.tBox_Ship_Name.Name = "tBox_Ship_Name";
            this.tBox_Ship_Name.Size = new System.Drawing.Size(242, 26);
            this.tBox_Ship_Name.TabIndex = 2;
            this.tBox_Ship_Name.TextChanged += new System.EventHandler(this.tBox_Ship_Name_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Ship:";
            // 
            // BTN_Search
            // 
            this.BTN_Search.Location = new System.Drawing.Point(328, 51);
            this.BTN_Search.Name = "BTN_Search";
            this.BTN_Search.Size = new System.Drawing.Size(162, 35);
            this.BTN_Search.TabIndex = 5;
            this.BTN_Search.Text = "Search and Select";
            this.BTN_Search.UseVisualStyleBackColor = true;
            this.BTN_Search.Click += new System.EventHandler(this.BTN_Search_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(234, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "HE penetration without IFHE";
            // 
            // HE_Pen_No_IFHE
            // 
            this.HE_Pen_No_IFHE.AutoSize = true;
            this.HE_Pen_No_IFHE.Location = new System.Drawing.Point(279, 137);
            this.HE_Pen_No_IFHE.Name = "HE_Pen_No_IFHE";
            this.HE_Pen_No_IFHE.Size = new System.Drawing.Size(145, 20);
            this.HE_Pen_No_IFHE.TabIndex = 7;
            this.HE_Pen_No_IFHE.Text = "HE_Pen_No_IFHE";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(240, 230);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(191, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "HE penetration with IFHE";
            // 
            // HE_Pen_IFHE
            // 
            this.HE_Pen_IFHE.AutoSize = true;
            this.HE_Pen_IFHE.Location = new System.Drawing.Point(279, 265);
            this.HE_Pen_IFHE.Name = "HE_Pen_IFHE";
            this.HE_Pen_IFHE.Size = new System.Drawing.Size(116, 20);
            this.HE_Pen_IFHE.TabIndex = 9;
            this.HE_Pen_IFHE.Text = "HE_Pen_IFHE";
            // 
            // IFHE_Tool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(520, 351);
            this.Controls.Add(this.HE_Pen_IFHE);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.HE_Pen_No_IFHE);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.BTN_Search);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tBox_Ship_Name);
            this.Controls.Add(this.Ship_List);
            this.Controls.Add(this.label1);
            this.Name = "IFHE_Tool";
            this.Text = "IFHE Tool";
            this.Load += new System.EventHandler(this.IFHE_Tool_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox Ship_List;
        private System.Windows.Forms.TextBox tBox_Ship_Name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BTN_Search;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label HE_Pen_No_IFHE;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label HE_Pen_IFHE;
    }
}