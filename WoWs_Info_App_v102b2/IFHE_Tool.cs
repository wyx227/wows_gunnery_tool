﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WoWs_Info_App
{
    public partial class IFHE_Tool : Form
    {
        public IFHE_Tool()
        {
            InitializeComponent();
        }

        private void IFHE_Tool_Load(object sender, EventArgs e)
        {
            HE_Pen_IFHE.Text = "";
            HE_Pen_No_IFHE.Text = "";
            XmlDocument xm = new XmlDocument();
            string list = "//Name";

            xm.Load(@".\ships.xml");
            XmlNodeList Xn = xm.SelectNodes(list);

            foreach (XmlNode xNode in Xn)
            {
                Ship_List.Items.Add(xNode.InnerText);
            }
        }

        private void BTN_Search_Click(object sender, EventArgs e)
        {
            XmlDocument xm = new XmlDocument();
            string[] parameter = new string[3];
            string list = "//Name";
            xm.Load(@".\ships.xml");
            XmlNodeList Xn = xm.SelectNodes(list);
            XmlNodeList Xn_Nation = xm.SelectNodes("//Nation");
            XmlNodeList Xn_Caliber = xm.SelectNodes("//Caliber");
            XmlNodeList Xn_Type = xm.SelectNodes("//Type");
            Ship_List.SelectedItems.Clear();
            for (int i = Ship_List.Items.Count - 1; i >= 0; i--)
            {
                if (Ship_List.Items[i].ToString().ToLower().Contains(tBox_Ship_Name.Text.ToLower()))
                {
                    Ship_List.SetSelected(i, true);

                }
            }
            for (int i = 0; i < Xn.Count; i++)
            {
                if (Ship_List.GetItemText(Ship_List.SelectedItem) == Xn[i].InnerText)
                {
                    parameter[0] = Xn_Caliber[i].InnerText;
                    parameter[1] = Xn_Nation[i].InnerText;
                    parameter[2] = Xn_Type[i].InnerText;
                }
            }
            calculate_penetration(parameter);
        }
        private void calculate_penetration(string[] parameter)
        {
            int Caliber = Int32.Parse(parameter[0]);
            double Penetration;
            string Nation = parameter[1];
            string Type = parameter[2];

            bool KMS = false;
            bool HMS = false;
            bool Cruiser = false;
            bool Battleship = false;

            if (Nation == "HMS")
            {
                HMS = true;
            }else if(Nation == "KMS")
            {
                KMS = true;
            }

            if (Type == "Cruiser")
            {
                Cruiser = true;
            }else if(Type == "Battleship")
            {
                Battleship = true;
            }

            if(KMS && (Cruiser | Battleship))
            {
                Penetration = Math.Floor(Caliber / 4.0);
                

            }else if(HMS && Battleship)
            {
                Penetration = Math.Floor(Caliber / 4.0);
            }
            else
            {
                Penetration = Math.Floor(Caliber / 6.0);
            }
            HE_Pen_No_IFHE.Text = Penetration.ToString()+" mm";
            HE_Pen_IFHE.Text = Math.Ceiling(Penetration * 1.3).ToString()+" mm";

        }

        private void IFHE_Tool_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
        }

        private void tBox_Ship_Name_TextChanged(object sender, EventArgs e)
        {
            XmlDocument xm = new XmlDocument();
            string[] parameter = new string[3];
            string list = "//Name";
            xm.Load(@".\ships.xml");
            XmlNodeList Xn = xm.SelectNodes(list);
            Ship_List.SelectedItems.Clear();
            for (int i = Ship_List.Items.Count - 1; i >= 0; i--)
            {
                if (Ship_List.Items[i].ToString().ToLower().Contains(tBox_Ship_Name.Text.ToLower()))
                {
                    Ship_List.SetSelected(i, true);

                }
            }
        }
    }
}
