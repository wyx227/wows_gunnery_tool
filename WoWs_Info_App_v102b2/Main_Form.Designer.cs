﻿namespace WoWs_Info_App
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.BTN_Search = new System.Windows.Forms.Button();
            this.Ship_List = new System.Windows.Forms.ListBox();
            this.tbox_Range = new System.Windows.Forms.TextBox();
            this.BTN_Calc = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Result_Vh = new System.Windows.Forms.Label();
            this.Pen = new System.Windows.Forms.Label();
            this.Ho_Pen = new System.Windows.Forms.Label();
            this.L_A = new System.Windows.Forms.Label();
            this.H_A = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Ver_Pen = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.functionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateWithYourOwnParametersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iFHEToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showShellDatasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comparingPenetrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.armourCalculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advancedSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeNaturalConstantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeCalculatingAccuracyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Version_Label = new System.Windows.Forms.Label();
            this.chkBox_Export = new System.Windows.Forms.CheckBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SearchBox
            // 
            this.SearchBox.Location = new System.Drawing.Point(108, 38);
            this.SearchBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(212, 26);
            this.SearchBox.TabIndex = 1;
            // 
            // BTN_Search
            // 
            this.BTN_Search.Location = new System.Drawing.Point(36, 73);
            this.BTN_Search.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BTN_Search.Name = "BTN_Search";
            this.BTN_Search.Size = new System.Drawing.Size(212, 46);
            this.BTN_Search.TabIndex = 2;
            this.BTN_Search.Text = "Search and Select";
            this.BTN_Search.UseVisualStyleBackColor = true;
            this.BTN_Search.Click += new System.EventHandler(this.BTN_Search_Click);
            // 
            // Ship_List
            // 
            this.Ship_List.FormattingEnabled = true;
            this.Ship_List.ItemHeight = 20;
            this.Ship_List.Location = new System.Drawing.Point(13, 162);
            this.Ship_List.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Ship_List.Name = "Ship_List";
            this.Ship_List.Size = new System.Drawing.Size(198, 384);
            this.Ship_List.TabIndex = 3;
            // 
            // tbox_Range
            // 
            this.tbox_Range.Location = new System.Drawing.Point(74, 128);
            this.tbox_Range.Name = "tbox_Range";
            this.tbox_Range.Size = new System.Drawing.Size(125, 26);
            this.tbox_Range.TabIndex = 4;
            // 
            // BTN_Calc
            // 
            this.BTN_Calc.Location = new System.Drawing.Point(296, 74);
            this.BTN_Calc.Name = "BTN_Calc";
            this.BTN_Calc.Size = new System.Drawing.Size(149, 45);
            this.BTN_Calc.TabIndex = 6;
            this.BTN_Calc.Text = "Calculate";
            this.BTN_Calc.UseVisualStyleBackColor = true;
            this.BTN_Calc.Click += new System.EventHandler(this.BTN_Select_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Range ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(266, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(179, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Effective Horizontal Pen";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(229, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Velocity at Hit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(381, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 20);
            this.label4.TabIndex = 13;
            this.label4.Text = "Penetration";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(218, 418);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "Lauching angle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(359, 418);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 20);
            this.label6.TabIndex = 15;
            this.label6.Text = "Hitting angle";
            // 
            // Result_Vh
            // 
            this.Result_Vh.AutoSize = true;
            this.Result_Vh.Location = new System.Drawing.Point(241, 162);
            this.Result_Vh.Name = "Result_Vh";
            this.Result_Vh.Size = new System.Drawing.Size(79, 20);
            this.Result_Vh.TabIndex = 16;
            this.Result_Vh.Text = "Result Vh";
            // 
            // Pen
            // 
            this.Pen.AutoSize = true;
            this.Pen.Location = new System.Drawing.Point(388, 162);
            this.Pen.Name = "Pen";
            this.Pen.Size = new System.Drawing.Size(37, 20);
            this.Pen.TabIndex = 17;
            this.Pen.Text = "Pen";
            // 
            // Ho_Pen
            // 
            this.Ho_Pen.AutoSize = true;
            this.Ho_Pen.Location = new System.Drawing.Point(310, 259);
            this.Ho_Pen.Name = "Ho_Pen";
            this.Ho_Pen.Size = new System.Drawing.Size(67, 20);
            this.Ho_Pen.TabIndex = 18;
            this.Ho_Pen.Text = "Ho_Pen";
            // 
            // L_A
            // 
            this.L_A.AutoSize = true;
            this.L_A.Location = new System.Drawing.Point(250, 447);
            this.L_A.Name = "L_A";
            this.L_A.Size = new System.Drawing.Size(38, 20);
            this.L_A.TabIndex = 19;
            this.L_A.Text = "L_A";
            // 
            // H_A
            // 
            this.H_A.AutoSize = true;
            this.H_A.Location = new System.Drawing.Point(388, 447);
            this.H_A.Name = "H_A";
            this.H_A.Size = new System.Drawing.Size(41, 20);
            this.H_A.TabIndex = 20;
            this.H_A.Text = "H_A";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(241, 310);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(214, 20);
            this.label7.TabIndex = 21;
            this.label7.Text = "Effective Vertical Penetration";
            // 
            // Ver_Pen
            // 
            this.Ver_Pen.AutoSize = true;
            this.Ver_Pen.Location = new System.Drawing.Point(310, 342);
            this.Ver_Pen.Name = "Ver_Pen";
            this.Ver_Pen.Size = new System.Drawing.Size(71, 20);
            this.Ver_Pen.TabIndex = 22;
            this.Ver_Pen.Text = "Ver_Pen";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.functionsToolStripMenuItem,
            this.advancedSettingsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(484, 33);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dataToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // dataToolStripMenuItem
            // 
            this.dataToolStripMenuItem.Name = "dataToolStripMenuItem";
            this.dataToolStripMenuItem.Size = new System.Drawing.Size(133, 30);
            this.dataToolStripMenuItem.Text = "Data";
            this.dataToolStripMenuItem.Click += new System.EventHandler(this.dataToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(133, 30);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // functionsToolStripMenuItem
            // 
            this.functionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.calculateWithYourOwnParametersToolStripMenuItem,
            this.iFHEToolToolStripMenuItem,
            this.showShellDatasToolStripMenuItem,
            this.comparingPenetrationToolStripMenuItem,
            this.armourCalculatorToolStripMenuItem});
            this.functionsToolStripMenuItem.Name = "functionsToolStripMenuItem";
            this.functionsToolStripMenuItem.Size = new System.Drawing.Size(100, 29);
            this.functionsToolStripMenuItem.Text = "Functions";
            // 
            // calculateWithYourOwnParametersToolStripMenuItem
            // 
            this.calculateWithYourOwnParametersToolStripMenuItem.Name = "calculateWithYourOwnParametersToolStripMenuItem";
            this.calculateWithYourOwnParametersToolStripMenuItem.Size = new System.Drawing.Size(378, 30);
            this.calculateWithYourOwnParametersToolStripMenuItem.Text = "Calculate with your own parameters";
            this.calculateWithYourOwnParametersToolStripMenuItem.Click += new System.EventHandler(this.calculateWithYourOwnParametersToolStripMenuItem_Click);
            // 
            // iFHEToolToolStripMenuItem
            // 
            this.iFHEToolToolStripMenuItem.Name = "iFHEToolToolStripMenuItem";
            this.iFHEToolToolStripMenuItem.Size = new System.Drawing.Size(378, 30);
            this.iFHEToolToolStripMenuItem.Text = "IFHE tool";
            this.iFHEToolToolStripMenuItem.Click += new System.EventHandler(this.iFHEToolToolStripMenuItem_Click);
            // 
            // showShellDatasToolStripMenuItem
            // 
            this.showShellDatasToolStripMenuItem.Name = "showShellDatasToolStripMenuItem";
            this.showShellDatasToolStripMenuItem.Size = new System.Drawing.Size(378, 30);
            this.showShellDatasToolStripMenuItem.Text = "Show shell datas";
            this.showShellDatasToolStripMenuItem.Click += new System.EventHandler(this.showShellDatasToolStripMenuItem_Click);
            // 
            // comparingPenetrationToolStripMenuItem
            // 
            this.comparingPenetrationToolStripMenuItem.Name = "comparingPenetrationToolStripMenuItem";
            this.comparingPenetrationToolStripMenuItem.Size = new System.Drawing.Size(378, 30);
            this.comparingPenetrationToolStripMenuItem.Text = "Comparing penetration";
            this.comparingPenetrationToolStripMenuItem.Click += new System.EventHandler(this.comparingPenetrationToolStripMenuItem_Click);
            // 
            // armourCalculatorToolStripMenuItem
            // 
            this.armourCalculatorToolStripMenuItem.Enabled = false;
            this.armourCalculatorToolStripMenuItem.Name = "armourCalculatorToolStripMenuItem";
            this.armourCalculatorToolStripMenuItem.Size = new System.Drawing.Size(378, 30);
            this.armourCalculatorToolStripMenuItem.Text = "Armour Calculator";
            this.armourCalculatorToolStripMenuItem.Click += new System.EventHandler(this.armourCalculatorToolStripMenuItem_Click);
            // 
            // advancedSettingsToolStripMenuItem
            // 
            this.advancedSettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeNaturalConstantsToolStripMenuItem,
            this.changeCalculatingAccuracyToolStripMenuItem});
            this.advancedSettingsToolStripMenuItem.Name = "advancedSettingsToolStripMenuItem";
            this.advancedSettingsToolStripMenuItem.Size = new System.Drawing.Size(172, 29);
            this.advancedSettingsToolStripMenuItem.Text = "Advanced Settings";
            // 
            // changeNaturalConstantsToolStripMenuItem
            // 
            this.changeNaturalConstantsToolStripMenuItem.Name = "changeNaturalConstantsToolStripMenuItem";
            this.changeNaturalConstantsToolStripMenuItem.Size = new System.Drawing.Size(316, 30);
            this.changeNaturalConstantsToolStripMenuItem.Text = "Change natural constants";
            this.changeNaturalConstantsToolStripMenuItem.Click += new System.EventHandler(this.changeNaturalConstantsToolStripMenuItem_Click);
            // 
            // changeCalculatingAccuracyToolStripMenuItem
            // 
            this.changeCalculatingAccuracyToolStripMenuItem.Name = "changeCalculatingAccuracyToolStripMenuItem";
            this.changeCalculatingAccuracyToolStripMenuItem.Size = new System.Drawing.Size(316, 30);
            this.changeCalculatingAccuracyToolStripMenuItem.Text = "Change calculating accuracy";
            this.changeCalculatingAccuracyToolStripMenuItem.Click += new System.EventHandler(this.changeCalculatingAccuracyToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(61, 29);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(146, 30);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(205, 134);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(22, 20);
            this.label8.TabIndex = 24;
            this.label8.Text = "m";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 20);
            this.label9.TabIndex = 25;
            this.label9.Text = "Ship Name:";
            // 
            // Version_Label
            // 
            this.Version_Label.AutoSize = true;
            this.Version_Label.Location = new System.Drawing.Point(228, 538);
            this.Version_Label.Name = "Version_Label";
            this.Version_Label.Size = new System.Drawing.Size(67, 20);
            this.Version_Label.TabIndex = 26;
            this.Version_Label.Text = "Version:";
            // 
            // chkBox_Export
            // 
            this.chkBox_Export.AutoSize = true;
            this.chkBox_Export.Location = new System.Drawing.Point(242, 509);
            this.chkBox_Export.Name = "chkBox_Export";
            this.chkBox_Export.Size = new System.Drawing.Size(141, 24);
            this.chkBox_Export.TabIndex = 27;
            this.chkBox_Export.Text = "Export to Excel";
            this.chkBox_Export.UseVisualStyleBackColor = true;
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(484, 567);
            this.Controls.Add(this.chkBox_Export);
            this.Controls.Add(this.Version_Label);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.Ver_Pen);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.H_A);
            this.Controls.Add(this.L_A);
            this.Controls.Add(this.Ho_Pen);
            this.Controls.Add(this.Pen);
            this.Controls.Add(this.Result_Vh);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BTN_Calc);
            this.Controls.Add(this.tbox_Range);
            this.Controls.Add(this.Ship_List);
            this.Controls.Add(this.BTN_Search);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "Main_Form";
            this.Text = "WoWs Shell Tool";
            this.Load += new System.EventHandler(this.Main_Form_Load_1);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.Button BTN_Search;
        private System.Windows.Forms.ListBox Ship_List;
        private System.Windows.Forms.TextBox tbox_Range;
        private System.Windows.Forms.Button BTN_Calc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label Result_Vh;
        private System.Windows.Forms.Label Pen;
        private System.Windows.Forms.Label Ho_Pen;
        private System.Windows.Forms.Label L_A;
        private System.Windows.Forms.Label H_A;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Ver_Pen;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem functionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateWithYourOwnParametersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iFHEToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showShellDatasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem comparingPenetrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advancedSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeNaturalConstantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeCalculatingAccuracyToolStripMenuItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ToolStripMenuItem armourCalculatorToolStripMenuItem;
        private System.Windows.Forms.Label Version_Label;
        private System.Windows.Forms.CheckBox chkBox_Export;
    }
}