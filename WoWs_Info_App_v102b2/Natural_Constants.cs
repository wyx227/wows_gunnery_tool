﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WoWs_Info_App
{
    public partial class Natural_Constants : Form
    {
        DataTable table = new DataTable("tbl");
        public Natural_Constants()
        {
            InitializeComponent();
        }

        private void Natural_Constants_Load(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            dataGridView1.DataSource = table;
            table.Columns.Add("Constant Name", typeof(string));
            table.Columns.Add("Value", typeof(double));
            ds.ReadXml(@".\constants.xml");
            dataGridView1.DataSource = ds.Tables[0];
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)dataGridView1.DataSource;
            dt.WriteXml(@".\constants.xml");
        }

        private void BTN_Cancel_Click(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
