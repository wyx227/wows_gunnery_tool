﻿//
// New features in version 1.0.2 Beta:
//  1. The programe is capable of outputing the calculation to an excel table, so that the user can create graphics with the data.
//  2. Shell normalisation is implemented in this version.
// ***Fix a bug, in which the bouncing angle of destroyers is not given correctly***
// ***From this version, the programe is capable of handling all parameters to increase calculation accuracy.
// ***It is impossible to choose between dot and comma spontaneously.
//  Known bugs:
//  
//



using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WoWs_Info_App
{
    static class Program
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main_Form());
        }
    }
}
