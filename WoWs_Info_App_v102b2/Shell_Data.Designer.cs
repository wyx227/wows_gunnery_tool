﻿namespace WoWs_Info_App
{
    partial class Shell_Data
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.tBox_Name = new System.Windows.Forms.TextBox();
            this.tBox_Nation = new System.Windows.Forms.TextBox();
            this.tBox_Type = new System.Windows.Forms.TextBox();
            this.tBox_Caliber = new System.Windows.Forms.TextBox();
            this.tBox_Mass = new System.Windows.Forms.TextBox();
            this.tBox_V = new System.Windows.Forms.TextBox();
            this.tBox_Krupp = new System.Windows.Forms.TextBox();
            this.tBox_D = new System.Windows.Forms.TextBox();
            this.BTN_Pre = new System.Windows.Forms.Button();
            this.BTN_Next = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(479, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Here you can check the shell parameters of every ship one by one. ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(399, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "To change them, use the built-in data management tool";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ship Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(161, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(274, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(393, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Caliber";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Shell Weight";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(144, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Initial Velocity";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(274, 194);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 20);
            this.label9.TabIndex = 8;
            this.label9.Text = "Krupp";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(393, 194);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(67, 20);
            this.label.TabIndex = 9;
            this.label.Text = "Air Drag";
            // 
            // tBox_Name
            // 
            this.tBox_Name.Location = new System.Drawing.Point(17, 116);
            this.tBox_Name.Name = "tBox_Name";
            this.tBox_Name.Size = new System.Drawing.Size(103, 26);
            this.tBox_Name.TabIndex = 10;
            // 
            // tBox_Nation
            // 
            this.tBox_Nation.Location = new System.Drawing.Point(136, 116);
            this.tBox_Nation.Name = "tBox_Nation";
            this.tBox_Nation.Size = new System.Drawing.Size(103, 26);
            this.tBox_Nation.TabIndex = 11;
            // 
            // tBox_Type
            // 
            this.tBox_Type.Location = new System.Drawing.Point(258, 116);
            this.tBox_Type.Name = "tBox_Type";
            this.tBox_Type.Size = new System.Drawing.Size(103, 26);
            this.tBox_Type.TabIndex = 12;
            // 
            // tBox_Caliber
            // 
            this.tBox_Caliber.Location = new System.Drawing.Point(377, 116);
            this.tBox_Caliber.Name = "tBox_Caliber";
            this.tBox_Caliber.Size = new System.Drawing.Size(103, 26);
            this.tBox_Caliber.TabIndex = 13;
            // 
            // tBox_Mass
            // 
            this.tBox_Mass.Location = new System.Drawing.Point(17, 217);
            this.tBox_Mass.Name = "tBox_Mass";
            this.tBox_Mass.Size = new System.Drawing.Size(103, 26);
            this.tBox_Mass.TabIndex = 14;
            // 
            // tBox_V
            // 
            this.tBox_V.Location = new System.Drawing.Point(136, 217);
            this.tBox_V.Name = "tBox_V";
            this.tBox_V.Size = new System.Drawing.Size(103, 26);
            this.tBox_V.TabIndex = 15;
            // 
            // tBox_Krupp
            // 
            this.tBox_Krupp.Location = new System.Drawing.Point(258, 217);
            this.tBox_Krupp.Name = "tBox_Krupp";
            this.tBox_Krupp.Size = new System.Drawing.Size(103, 26);
            this.tBox_Krupp.TabIndex = 16;
            // 
            // tBox_D
            // 
            this.tBox_D.Location = new System.Drawing.Point(377, 217);
            this.tBox_D.Name = "tBox_D";
            this.tBox_D.Size = new System.Drawing.Size(103, 26);
            this.tBox_D.TabIndex = 17;
            // 
            // BTN_Pre
            // 
            this.BTN_Pre.Location = new System.Drawing.Point(181, 263);
            this.BTN_Pre.Name = "BTN_Pre";
            this.BTN_Pre.Size = new System.Drawing.Size(123, 54);
            this.BTN_Pre.TabIndex = 18;
            this.BTN_Pre.Text = "<< Previous";
            this.BTN_Pre.UseVisualStyleBackColor = true;
            this.BTN_Pre.Click += new System.EventHandler(this.BTN_Pre_Click);
            // 
            // BTN_Next
            // 
            this.BTN_Next.Location = new System.Drawing.Point(310, 263);
            this.BTN_Next.Name = "BTN_Next";
            this.BTN_Next.Size = new System.Drawing.Size(123, 54);
            this.BTN_Next.TabIndex = 19;
            this.BTN_Next.Text = "Next >>";
            this.BTN_Next.UseVisualStyleBackColor = true;
            this.BTN_Next.Click += new System.EventHandler(this.BTN_Next_Click);
            // 
            // Shell_Data
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(517, 329);
            this.Controls.Add(this.BTN_Next);
            this.Controls.Add(this.BTN_Pre);
            this.Controls.Add(this.tBox_D);
            this.Controls.Add(this.tBox_Krupp);
            this.Controls.Add(this.tBox_V);
            this.Controls.Add(this.tBox_Mass);
            this.Controls.Add(this.tBox_Caliber);
            this.Controls.Add(this.tBox_Type);
            this.Controls.Add(this.tBox_Nation);
            this.Controls.Add(this.tBox_Name);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "Shell_Data";
            this.Text = "Shell_Data";
            this.Load += new System.EventHandler(this.Shell_Data_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TextBox tBox_Name;
        private System.Windows.Forms.TextBox tBox_Nation;
        private System.Windows.Forms.TextBox tBox_Type;
        private System.Windows.Forms.TextBox tBox_Caliber;
        private System.Windows.Forms.TextBox tBox_Mass;
        private System.Windows.Forms.TextBox tBox_V;
        private System.Windows.Forms.TextBox tBox_Krupp;
        private System.Windows.Forms.TextBox tBox_D;
        private System.Windows.Forms.Button BTN_Pre;
        private System.Windows.Forms.Button BTN_Next;
    }
}