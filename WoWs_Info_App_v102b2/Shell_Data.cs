﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WoWs_Info_App
{
    public partial class Shell_Data : Form
    {
        int index = 0;
        int length;

        public Shell_Data()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void Shell_Data_Load(object sender, EventArgs e)
        {
            Display_Data(index);
        }

        private void Display_Data(int i)
        {
            XmlDocument xm = new XmlDocument();
            xm.Load(@".\ships.xml");
            XmlNodeList Xn = xm.SelectNodes("//Name");
            XmlNodeList Xn_Caliber = xm.SelectNodes("//Caliber");
            XmlNodeList Xn_Initial_V = xm.SelectNodes("//Initial_Velocity");
            XmlNodeList Xn_Shell_W = xm.SelectNodes("//Shell_Weight");
            XmlNodeList Xn_Air_Drag = xm.SelectNodes("//Air_Drag");
            XmlNodeList Xn_Krupp = xm.SelectNodes("//Krupp");
            XmlNodeList Xn_Nation = xm.SelectNodes("//Nation");
            XmlNodeList Xn_Type = xm.SelectNodes("//Type");
            length = Xn.Count;
            tBox_Name.Text = Xn[i].InnerText;
            tBox_Nation.Text = Xn_Nation[i].InnerText;
            tBox_Type.Text = Xn_Type[i].InnerText;
            tBox_Caliber.Text = Xn_Caliber[i].InnerText;
            tBox_V.Text = Xn_Initial_V[i].InnerText;
            tBox_Mass.Text = Xn_Shell_W[i].InnerText;
            tBox_Krupp.Text = Xn_Krupp[i].InnerText;
            tBox_D.Text = Xn_Air_Drag[i].InnerText;

        }

        private void BTN_Pre_Click(object sender, EventArgs e)
        {
            if (index == 0)
            {
                MessageBox.Show("Already the first record!");
            }
            else
            {
                index = index - 1;
            }
            

            Display_Data(index);
        }

        private void BTN_Next_Click(object sender, EventArgs e)
        {
            if (index == length -1)
            {
                MessageBox.Show("Already the last record!");
            }
            else
            {
                index = index + 1;
            }


            Display_Data(index);
        }
    }
}
